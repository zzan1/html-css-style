const burger = document.querySelector(".burger");
const navBar = document.querySelector("nav");
const navLi = document.querySelectorAll("nav ul li");
// querySelector 不能通过 nav.open 来选择

burger.addEventListener("click", () => {
  // toggle 添加，有就不添加，没有就添加
  burger.classList.toggle("active");
  navBar.classList.toggle("open");
  navLi.forEach((el, index) => {
    // 设置 html 中相应标签的 style 属性，相当于在 css 中添加了一个 属性：值 对
    if (el.style.animation) {
      el.style.animation = "";
    } else {
      el.style.animation = `slideIn 0.3s ease-in forwards ${index * 0.1 +
        0.3}s`;
    }
  });
});
